'use client'
import React, { useState, useEffect, useRef } from 'react';
import { ApiUrl } from '../../../api/login/index';
import fetchData from '@/app/hooks/fetchdata';
import Robot  from "../../../svg/robot";
interface Data {
  input_message: string;
  ai_response: string;
}
const ApiCallComponent = () => {
  const [inputMessage, setInputMessage] = useState('');
  const [apiResponse, setApiResponse] = useState('');

  const [msgdata, setMsgdata] = useState([]);
  const containerRef = useRef<HTMLDivElement>(null);


  const fetchAndSetData = async () => {
    const resultdata = await fetchData('last');
    const data = resultdata.data
    setMsgdata(data);
    if (containerRef.current) {
      containerRef.current.scrollTop = containerRef.current.scrollHeight;
    }
  };
  useEffect(() => {

    fetchAndSetData();

  }, [apiResponse]);
  useEffect(() => {
    // Scroll to the bottom when msgdata changes
    if (containerRef.current) {
      containerRef.current.scrollTop = containerRef.current.scrollHeight;
    }
  }, [msgdata]);

  const handleInputChange = (e: any) => {
    setInputMessage(e.target.value);
  };
  const handleOnClick = (value: string) => {
    setInputMessage(() => {
      return value;
    });

    setTimeout(() => {
      if (inputMessage) {
        handleApiCall();
      }
    }, 0);
  };
  const handleApiCall = async () => {
    try {
      if (!inputMessage) {
        // console.log("Input message is empty. Skipping API call.");
        return;
      }

      const response = await fetch(ApiUrl, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ message: inputMessage }),
      });

      if (!response.ok) {
        console.log(inputMessage);
        throw new Error('Error in API call');
      }

      const responseData = await response.json();
      const responseMsg = responseData.response;
      setApiResponse(responseMsg);
    } catch (error) {
      console.error('API call error:', error);
    }
    setInputMessage("");
  };

  const handleKeyDown = (event: any) => {
    if (event.key === 'Enter') {
      console.log("enter clicked")
      handleApiCall();
    }
  };

  return (
    <div className="container items-center flex  h-screen">
      <div className="max-w-md mx-auto p-4 bg-gray-100 shadow-lg rounded-md ">
        <div className='h-80 overflow-auto' ref={containerRef}>
          {msgdata && (
            <>
              {msgdata.map((msg: Data, index: number) =>
                <section className='' key={index}>
                  <div className='flex justify-end row py-2 bg-slate-50'>
                    <label htmlFor="" className='px-6 py-2 rounded-full bg-blue-500'>{msg.input_message}: You</label>
                  </div>

                  <div className='flex row py-2 bg-slate-50 '>
                    <div className='flex row px-3 py-2 rounded-full bg-gray-200 gap-1 items-center'>
                      <Robot/>
                      <label htmlFor="" className=''> 
                        AI: {msg.ai_response}
                      </label>
                    </div>
                   
                  </div>


                </section>
              )}
            </>
          )}
        </div>


        <label className="block mb-2">
          Enter Message:
        </label>
        <div className='flex row gap-1'>

          <input
            type="text"
            value={inputMessage}
            onChange={handleInputChange}

            onKeyDown={handleKeyDown}
            className="w-full px-3 py-2 border rounded-md"
            autoFocus
          />
          <button
            onClick={handleApiCall}
            className=" text-white px-3 bg-blue-500  rounded-md hover:bg-blue-600"
          >Send
          </button>
        </div>

        <div className="mt-4 space-x-2">
          <button
            onClick={() => handleOnClick('hello')}
            className="bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-600"
          >
            Hello
          </button>
          <button
            onClick={() => handleOnClick('how are you')}
            className="bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-600"
          >
            How are you?
          </button>
        </div>
      </div>
    </div>

  );
};

export default ApiCallComponent;
