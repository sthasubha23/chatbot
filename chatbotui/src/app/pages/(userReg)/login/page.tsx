'use client'
import React,{useState} from 'react'

function login() {
    const [username,setUsername] = useState('');
    const [password,setPassword] = useState('');

    const handleSubmit = (event:any) => {
        const formData =  new FormData(event.target);
        const enteredUsername= formData.get("username");
        const enteredPassword= formData.get("password");

        if (enteredUsername && enteredPassword !== null) {
            setUsername(enteredUsername.toString());
            setPassword(enteredPassword.toString());
        }
        console.log("Submit button Clicked",enteredUsername,enteredPassword);
    }
    return (
        <>
            <form onSubmit={handleSubmit}>
                <input className='border-1' type="text" name='username' />
                <input className='border-1' type="password" name="password" />
                <button type='submit'>Login</button>
            </form>
            <div>
                <label htmlFor="">{username}</label>
                <label htmlFor="">{password}</label>
            </div>
        </>
    )
}

export default login