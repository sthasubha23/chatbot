import { API_HOST } from "../api/configs/page";
import React from 'react'
export default async function fetchData(filter:string) {
    try {
      const response = await fetch(`${API_HOST}/api/v1/getchat/?page=${filter}`);
      console.log(response);
      
      if (!response.ok) {
        throw new Error(`HTTP error!: Status: ${response.status}`);
      }
      
      const data = await response.json();
    //   console.log(data.results);
  
      return data.results;
    } catch (error: any) {
        // console.log(error,'here is the error')
      return [error.message];
    }
  }

  // https://api.themoviedb.org/3/search/movie
  