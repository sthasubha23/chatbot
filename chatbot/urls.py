from django.urls import path
from .views import chat,ChatApi

urlpatterns = [
    path('chat/',chat, name='chat'),
    path('getchat/',ChatApi.as_view(),name='list')
]
