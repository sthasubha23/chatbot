from django.db import models

# Create your models here.
class Chats(models.Model):
    input_message = models.CharField(max_length=150,null=True,blank=True)
    ai_response = models.CharField(max_length=150,null=True,blank=True)
    created_at = models.DateTimeField(auto_now=True, auto_now_add=False)